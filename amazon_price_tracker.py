import requests
import smtplib
from bs4 import BeautifulSoup
import lxml

URL = "AMAZON URL FOR THE ITEM YOU WANT"
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36",
    "Accept-Language": "en-US,en;q=0.9"
}
min_price =  0 # SET MINIMUM PRICE BEYOND WHICH YOU WANT TO BE NOTIFIED

response = requests.get(URL, headers=headers)
response.raise_for_status()

website = response.text
soup = BeautifulSoup(website, 'lxml') #USES LXML PARSER INSTEAD OF HTML.PARSER

try:
    item_name = price = soup.find(
        name="span", id="productTitle").getText().strip()
except AttributeError:
    print("No item found")
else:
    try:
        price = soup.find(name="span", id="priceblock_ourprice").getText()
    except AttributeError:
        print("No price found")
    else:
        price = float(price.strip("$"))


def is_cheap(price):
    if price < min_price:
        return True
    return False


def send_mail(item_name, price, URL):
    gmail_user = "YOUR EMAIL"
    gmail_password = "YOUR PASSWORD"
    recepient = "EMAIL OF THE RECEPIENT"
    mail = f"{item_name} is now selling for ${price}!\n\n{URL}"
    try:
        with smtplib.SMTP('smtp.gmail.com', 587) as connection:
            connection.starttls()
            connection.login(gmail_user, gmail_password)
            connection.sendmail(
                gmail_user, recepient, f"Subject:Amazon Price Alert!\n\n{mail}")
    except:
        print("Email failed")
    else:
        print(
            f"Successfully sent email")


if is_cheap(price):
    print(item_name)
    send_mail(item_name, price, URL)
